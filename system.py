import os


# OS

def getfile(path):
    try:
        f = open(path)
        lines = f.read()
        f.close()
        return lines
    except IOError: return ""

def as_root():
    if os.getuid() == 0: return True

def os_which(binary):
    for p in os.environ.get("PATH", "").split(":"):
        path = "%s/%s" % (p, binary)
        if os.path.isfile(path): return path
    return binary

