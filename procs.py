# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import popen2
import os
import signal
import string
import sys
import time

from conf import config
from output import logger
import regex
import system


# Manage processes

def execute(cmd):
    process = popen2.Popen3(cmd, True)
    sout = process.fromchild
    serr = process.childerr
    code = process.wait()
    return (code, sout, serr)

def run(cmd):
    #logger.display("+ " + cmd, log=False)
    cmd = "LC_ALL=C %s" % cmd
    logger.log("+ %s" % cmd)
    (code, sout, serr) = execute(cmd)
    o = ""
    if code != 0:
        e = serr.read().strip()
        if e: logger.err(e)
    else:
        o = sout.read()
        logger.log(o)
    return o

def kill_by_name(proc):
    cmd = "%s %s" % (config.pgrep, os.path.basename(proc))
    out = run(cmd).strip()
    deadline = time.time()+0.3
    while out and time.time() < deadline:
        pids = string.split(out, "\n")
        for p in pids:
            pid = -1
            try: pid = int(p)
            except ValueError:
                logger.err("Cast of pid int('%s') for process '%s' failed" % (p, proc))
                return
            try:
                os.kill(pid, signal.SIGINT)
                os.kill(pid, signal.SIGTERM)
                os.kill(pid, signal.SIGKILL)
            except OSError:
                logger.err("Failed to kill process '%s' pid %d" % (proc, pid))
                return
        out = run(cmd).strip()

def kill_previous_instances():
    pids = system.getfile(config.pidfile).strip().split("\n")
    if not [x for x in pids if x != ""]: return

    # truncate pidfile
    try: f = open(config.pidfile, 'w'); f.close()
    except IOError:
        logger.err("Could not truncate pidfile %s" % config.pidfile, log=False)
    
    for pid in pids:
        pid = int(pid)
        out = run("%s %s" % (config.ps, pid))
        active = regex.find1("(%s)" % config.program_name, out)
        if out and active:
            os.kill(pid, signal.SIGINT)
            os.kill(pid, signal.SIGTERM)
            os.kill(pid, signal.SIGKILL)

def write_pid():
    try:
        f = open(config.pidfile, 'a')
        f.write("%s\n" % os.getpid())
        f.close()
    except IOError:
        logger.err("Could not write to pidfile %s" % config.pidfile, log=False)

def daemonize():
    # do unix double fork
    try:
        pid = os.fork()
        if pid > 0: sys.exit(0)
    except OSError:
        logger.err("Failed to daemonize", log=False)
        sys.exit(1)

    os.chdir("/")
    os.setsid()
    os.umask(0)
    child_in =open('/dev/null','r')
    child_out=open('/dev/null','w')
    os.dup2(child_in.fileno(), sys.stdin.fileno())
    os.dup2(child_out.fileno(), sys.stdout.fileno())
    os.dup2(child_out.fileno(), sys.stderr.fileno())

    try:
        pid = os.fork()
        if pid > 0: sys.exit(0)
    except OSError:
        logger.err("Failed to daemonize", log=False)
        sys.exit(1)
