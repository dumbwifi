#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import glob
import sys


verbose = 1
if len(sys.argv) > 1 and sys.argv[1] == "-v": verbose = 2

cmd_template="""
import %s
%s.run(verbose)
"""

tests = glob.glob("test_*.py")
for test in tests:
    test = test.replace(".py", "")
    cmd = (cmd_template % (test, test)).strip()
    exec(compile(cmd, "", "exec"))
