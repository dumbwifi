#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import sys
sys.path.append("..")

import unittest

from sdict import sdict, sdictlist


class TestSdictList(unittest.TestCase):

    def setUp(self):
        self.network1 = sdict()
        self.network1.essid = "linksys"
        self.network1.bssid = "11:22:33:44:55:66"
        self.network1.priority = 75

        self.network2 = sdict()
        self.network2.essid = "NETGEAR"

        self.network3 = sdict()
        self.network3.essid = "Cisco"
        self.network3.priority = 60
    
        self.lst = sdictlist(sort_key="priority")
        self.lst.append(self.network1)
        self.lst.append(self.network2)
        self.lst.append(self.network3)

    def test_init(self):
        dl = sdictlist(init_list=[self.network1, self.network2])
        self.assert_(len(dl) == 2)

    def test_ordering(self):
        self.assert_(self.lst.get_top().essid == "linksys")

    def test_sort(self):
        s = self.lst.sort(sort_key="bssid")
        self.assert_(s.get_top().essid == "linksys")

    def test_merge(self):
        network4 = sdict()
        network4.essid = "Sitecom"

        network5 = sdict()
        network5.essid = "Cisco"
        network5.priority = 90

        dl = sdictlist(init_list=[network4, network5])
        dl = self.lst.merge(dl, "essid")

        self.assert_(len(dl) == 4)
        self.assert_(dl.get_top().essid == "Cisco")


def run(verbosity=1):
    suite = unittest.makeSuite(TestSdictList)
    unittest.TextTestRunner(verbosity=verbosity).run(suite)

if __name__ == "__main__":
    run()
