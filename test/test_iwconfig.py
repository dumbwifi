#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import sys
sys.path.append("..")

import unittest

from conf import config
import network
from sdict import sdict
import ui


class TestIwconfig(unittest.TestCase):

    def setUp(self):
        ui.init_routine()

    def test_setaccesspoint(self):
        wifi_net = sdict()
        wifi_net.bssid = "11:22:33:44:55:66"
        wifi_net.essid = "my_ap"
        wifi_net.channel = 1
        wifi_net.key = "silly_____key"

        f = lambda x: x.medium == "wireless"
        iface_name = config.interfaces.get_top(pred=f).interface
        network.setup_wifi(iface_name, network=wifi_net)

        info = network.read_wifi(iface_name)
        self.assert_(info.mode == "managed")
#        self.assert_(info.bssid == wifi_net.bssid) # not associated
        self.assert_(info.essid == wifi_net.essid)
        self.assert_(info.key != None)


def run(verbosity=1):
    suite = unittest.makeSuite(TestIwconfig)
    unittest.TextTestRunner(verbosity=verbosity).run(suite)

if __name__ == "__main__":
    run()
