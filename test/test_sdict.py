#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import sys
sys.path.append("..")

import unittest

from sdict import sdict


class TestSdict(unittest.TestCase):

    def setUp(self):
        self.d = sdict( {
            'a': "bear",
            'b': 2,     # cast to string
            'c': "",    # the following should not be set
            'd': " ",
            'e': None
            } )

    def test__init__(self):
        self.assert_(self.d.a == "bear")
        self.assert_(self.d.b == "2")

    def test_setitem(self):
        self.d['x'] = "5"
        self.assert_(self.d['x'] == "5" and self.d.x == "5")

    def test_delitem(self):
        del(self.d['a'])
        self.assert_(self.d['a'] == None and self.d.a == None)


    def test_clear(self):
        self.d.clear()
        self.assert_(len(self.d) == 0)
        self.assert_(self.d.items() == [])

    def test_fromkeys(self):
        d = self.d.fromkeys(['x', 'y'], "boo")
        self.assert_(d.items() == [('y', 'boo'), ('x', 'boo')])

    def test_items(self):
        self.assert_(self.d.items() == [('a', "bear"), ('b', "2")])

    def test_keys(self):
        self.assert_(self.d.keys() == ['a', 'b'])

    def test_pop(self):
        self.d.pop('a')
        self.assert_(self.d.a == None)

    def test_popitem(self):
        items = self.d.items()
        l1 = len(self.d)
        item = self.d.popitem()
        l2 = len(self.d)
        self.assert_(item in items)
        self.assert_(l1 - 1 == l2)

    def test_setdefault(self):
        self.d.setdefault('x', "new")
        self.assert_(self.d.x == "new")

    def test_values(self):
        self.assert_(self.d.values() == ["bear", "2"])


    def test_setattr(self):
        self.d.x = "5"
        self.assert_(self.d['x'] == "5" and self.d.x == "5")

    def test_delattr(self):
        del(self.d.__dict__['a'])
        self.assert_(self.d['a'] == None and self.d.a == None)


def run(verbosity=1):
    suite = unittest.makeSuite(TestSdict)
    unittest.TextTestRunner(verbosity=verbosity).run(suite)

if __name__ == "__main__":
    run()
