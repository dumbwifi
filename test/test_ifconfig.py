#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import sys
sys.path.append("..")

import unittest

from conf import config
import network
import ui


class TestIfconfig(unittest.TestCase):

    def setUp(self):
        ui.init_routine()

    def test_bringup(self):
        iface_name = config.interfaces.get_top().interface
        ip = "1.2.3.4"
        network.iface_up(iface_name, ip=ip)
        info = network.read_iface(iface_name)
        self.assert_(info.up != None)
        self.assert_(network.has_ip(iface_name) == ip)

    def test_takedown(self):
        iface_name = config.interfaces.get_top().interface
        network.iface_down(iface_name)
        info = network.read_iface(iface_name)
        self.assert_(info.up == None)
        self.assert_(info.ip == None)


def run(verbosity=1):
    suite = unittest.makeSuite(TestIfconfig)
    unittest.TextTestRunner(verbosity=verbosity).run(suite)

if __name__ == "__main__":
    run()
