#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import os
import shutil
import sys
import time

from conf import config


class Logger:

    def __init__(self):
        self.dirty = False
        self.pending = None
        self.muted = False

    # output: => Requesting ip...
    def await(self, msg):
        if self.pending:
            raise Exception,\
            "Attempted to display new pending message without closing previous"

        msg = "=> %s..." % msg
        self.pending = msg
        self.dirty = False
        print msg,
        self.log(msg)
        sys.stdout.flush()

    # output: (=> Requesting ip...) done
    def result(self, msg):
        if not self.pending:
            raise Exception,\
            "Attempted to display result of action without pending message"
        
        if not self.dirty:
            print msg
            self.log("%s %s" % (self.pending, msg))
        else:
            msg = "%s %s" % (self.pending, msg)
            print "\r%s" % msg  # workaround for space at the start of the line
            self.log(msg)
        self.pending = None
        sys.stdout.flush()

    def display(self, msg, log=True):
        if self.pending and not self.dirty:
            self.dirty = True
            msg = "\n%s" % msg
        print msg
        if log: self.log(msg)
        sys.stdout.flush()

    def err(self, msg, log=True):
        msg = "Erratum: %s" % msg
        if self.pending:
            self.dirty = True
            print >>sys.stderr, "\n%s" % msg
        else: print >>sys.stderr, msg
        if log: self.log(msg)
        sys.stdout.flush()

    def log(self, msg):
        if self.muted: return
        try:
            # truncate logfile
            if os.path.isfile(config.logfile) and \
                os.path.getsize(config.logfile) > config.logfile_size:
                shutil.move(config.logfile, "%s.0" % config.logfile)

            t = time.strftime("%d.%m.%y %H:%M:%S", time.gmtime())
            
            f = open(config.logfile, 'a')
            if msg[-1:] != '\n': msg += '\n'
            f.write("[%s] %s" % (t, msg))
            f.close()
        except IOError:
            self.err("Could not write to logfile %s" % config.logfile, log=False)

    def mute(self, quiet=False):
        if self.muted: return
        if not quiet:
            self.display("*** Halting output until we need to obtain ip again")
        sys.stdin  = open("/dev/null", 'r')
        sys.stdout = open("/dev/null", 'w')
        sys.stderr = open("/dev/null", 'w')
        self.muted = True

    def unmute(self, quiet=False):
        if not self.muted: return
        sys.stdin  = sys.__stdin__
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        if not quiet: self.display("*** Resuming output")
        self.muted = False


logger = Logger()



def format_timespan(span):
    prefix = ''
    units = ['s', 'm', 'h', 'd', 'w']
    unit_id = 0

    # less than 1
    if span < 1:
        prefix = '<'
        span = 1

    # minutes
    if span > 59:
        span /= 60
        unit_id += 1
        # hours
        if span > 59:
            span /= 60
            unit_id += 1
            # days
            if span > 23:
                span /= 24
                unit_id += 1
                # weeks
                if span > 6:
                    span /= 7
                    unit_id += 1

    span = int(round(span))
    return "%s%s%s" % (prefix, span, units[unit_id])

