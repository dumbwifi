#!/usr/bin/env python

import curses
import os
import stat
import traceback

from conf import config
from output import logger
import network
import output
import system
import time
import sys


def init_routine():
    as_root_check()
    init_config()


def as_root_check():
    if not system.as_root():
        logger.err("Must run as root", log=False)
        sys.exit(1)


def init_config():
    try:
        # make sure config file is protected from user access
        mode = os.stat(config.configfile)[stat.ST_MODE]
        if mode & (stat.S_IRWXG | stat.S_IRWXO):
            logger.err(\
            "Config file %s should only be accessible by root (mode is: %s)"\
                % (config.configfile, oct(stat.S_IMODE(mode))))
            sys.exit(1)
        
        config.init_config()
    except Exception, msg:
        logger.err(msg)
        sys.exit(1)


def tools_check():
    logger.display("Checking for missing tools...", log=False)
    for cmd_name in config.tools:
        var_name = cmd_name.replace("-", "_")
        path = config.__dict__[var_name]
        if os.path.isabs(path):
            logger.display(" [ok] %s" % path, log=False)
        else:
            logger.display(" [missing] %s" % cmd_name, log=False)
    sys.exit(0)


def ip_check():
    logger.await("Checking for ip")
    ips = [(x, y) for (x, y) in
        [(i.interface, network.has_ip(i.interface))
            for i in config.interfaces]
        if y != None] # (iface, ip) pairs where ip is set
    if ips:
        logger.result(
            reduce(lambda x, y: "%s  %s" % (x, y),
            map(lambda (x, y): "%s (%s)" % (y, x), ips)))
        return True
    else:
        logger.result("none")


def choose_medium():
    logger.await("Selecting preferred network medium")
    iface = config.interfaces.get_top()
    logger.result(iface.medium)
    if iface.medium == "wired": return True


def check_wired_link():
    logger.await("Checking for wired link")
    ifaces = config.interfaces.get_all(pred=lambda x: x.medium == "wired")
    connected = [network.wire_connected(i.interface) for i in ifaces \
        if network.wire_connected(i.interface)]
    if connected:
        logger.result(reduce(lambda x,y: "%s %s" % (x,y),
            map(lambda x: "%s" % x, connected)))
        return True
    else:
        logger.result("none, use wireless")


def request_ip(iface, net=None, tries=3):
    n = iface.interface
    if net:
        n = "%s (from %s)" % (n, net.essid)
        if not network.setup_wifi(iface.interface, net):
            logger.err("Failed to associate with access point for network %s"\
                % net.essid)
            return

    attempt = 0
    while attempt < tries:
        attempt += 1
        logger.await("(%d/%d) Request ip on %s" % (attempt, tries, n))
        
        start_time = time.time()
        ip = network.request_ip(iface.interface)
        stop_time = time.time()
        duration = stop_time - start_time
        
        if ip:
            logger.result("%s (%ds)" % (ip, duration))
            return True
        else: logger.result("failed (%ds)" % duration)


def scan_wifi_networks(iface, wardrive=None):
    logger.await("Scan for wireless networks")
    nets = network.read_scan(network.normal_scan(iface.interface))
    nets = config.networks.merge(nets, 'essid')
    f = lambda x: x.priority != None and x.signal != None
    if wardrive:
        f = lambda x: x.signal != None and x.priority == None\
            and x.encrypted == None and x.essid != "<hidden>"
    nets = nets.sort(sort_key='quality').get_all(pred=f)
    if nets:
        logger.result(reduce(lambda x,y: "%s  %s" % (x,y),
            map(lambda x: "%s" % x.essid, nets[:3])))
        return nets
    else: logger.result("none")


def display_live_networks(nets, field=None, column=None):
    if not nets:
        return "No networks detected"
    
    keys = ['bssid', 'essid', 'channel', 'bitrate', 'sec', 'signal', 'last', 'freq']
    width = 19; mw = 4; sp = 2 # width: max width  mw : min width  sp : separate

    if field:
        nets = nets.sort(sort_key=field)
    elif column and 0 < column <= len(keys):
        nets = nets.sort(sort_key=keys[column-1])
    else:
        nets = nets.sort(sort_key='signal')
    
    def col_len(dcts, key):
        "figure out column length to assign to fields based on content length"
        l = []
        for dct in dcts:
            # ugly special case handling
            if key == 'bitrate':
                l.append(len('bitra')+sp)
                continue
            if key == 'last':
                l.append(len('last')+sp)
                continue
            if key == 'freq':
                l.append(len('freq')+sp)
                continue

            val = dct.get(key)
            if val:
                l.append(len(val)+sp)
            else:
                l.append(sp)
        return l
    ws = [max(min(max(col_len(nets, k)), width), mw) for k in keys]

    s = ""
    for (i, key) in enumerate(keys):
        s += ("%s" % key[:ws[i]-sp]).ljust(ws[i])
    s = s[:-sp] + "\n"
    for net in nets:
        for (i, key) in enumerate(keys):
            val = net.get(key)

            if val:
                # ugly special case handling
                if key == 'bitrate':
                    val = val.replace('/s', '')
                if key == 'last':
                    val = "%s" % output.format_timespan(time.time()-val)
                if key == 'freq':
                    val = int(round(100 * int(val) / config.scan_count))
                    if val < 1:
                        val = "<1%"
                    else:
                        val = "%s%%" % val

                s += (val[:ws[i]-sp]).ljust(ws[i])
            else:
                s += "".ljust(ws[i])
        s = s[:-sp] + "\n"
    return s + "%d network(s) found" % len(nets)


def curse(iface, timeout):
    def curses_init():
        logger.mute(quiet=True)

        scr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        scr.keypad(1)
        curses.curs_set(0)
        return scr

    def curses_shutdown(scr):
        curses.nocbreak()
        scr.keypad(0)
        curses.echo()
        curses.endwin()

        logger.unmute(quiet=True)

    def display_list(scr, header, status, list_s):
        if list_s:
            ss = list_s.split("\n")
            scr.clear()
            try:
                scr.addstr(header + " / Ctrl+C to exit\n")
                if status:
                    scr.addstr(status + "\n")
                scr.addstr("\n"+ss[0]+"\n", curses.A_REVERSE)

                # add one by one until possible exception is thrown
                # when the list is too long
                for s in ss[1:]:
                    scr.addstr(s+"\n")
            except:
                pass
            scr.refresh()

    def get_status(timeout, t):
        if timeout > 0:
            return "-> scanning for %ds (-%ds)" % (timeout, t-time.time())
        return ""

    list_s = ""
    t = time.time()+timeout
    header = "%s scanning for wireless networks (%s)" % \
            (config.program_name, iface)
    status = get_status(timeout, t)

    try:
        scr = curses_init()
        display_list(scr, header, status, display_live_networks(None))

        while True:

            try:
                status = get_status(timeout, t)
                if timeout > 0:
                    if t < time.time(): break

                scan_data = network.single_scan(iface)
                nets = network.read_scan(scan_data)
                list_s = display_live_networks(nets)

                display_list(scr, header, status, list_s)

                curses.napms(500)
            except OSError:
                pass    # don't bail out on terminal window resize

    except KeyboardInterrupt:
        curses_shutdown(scr)
    except:
        curses_shutdown(scr)
        traceback.print_stack()
        traceback.print_exc()
    else:
        curses_shutdown(scr)

    print list_s
