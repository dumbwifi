# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import re


# String matching

def matches(r, s):
    m = re.compile(r).search(s)
    if m: return True

def find1(r, s):
    if not s: return ""
    m = re.compile(r).search(s)
    if m and m.groups() and m.groups()[0]:
        return m.groups()[0]

def find(r, s):
    m = re.compile(r).search(s)
    if m: return m
        #return m.groups()

def findall(r, s):
    m = re.compile(r).findall(s)
    if m: return m

def replaceall(old, new, s):
    return re.sub(old, new, s)
