#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import os
import sys

from sdict import sdict, sdictlist
import regex
import system


class Config:
    def __init__(self):

        self.opts = sdict()

        self.interfaces = sdictlist(sort_key="priority")
        self.interface_media = ["wired", "wireless"]
        self.interface_fields = ["interface", "priority"]

        self.networks = sdictlist(sort_key="priority")
        self.network_fields = ["essid", "key", "priority", "vpn_exec"]

        self.scan_count = 0
        self.live_networks = sdictlist(sort_key="priority")

        # Constants
        self.max_priortiy = 255
        self.min_priortiy = 0

        # Runtime info
        self.program_name = "dumbwifi"
        self.configfile = "/etc/%s.conf" % self.program_name
        self.pidfile = "/var/run/%s.pids" % self.program_name

        # Logging
        self.logfile = "/var/log/%s" % self.program_name
        self.logfile_size = 10*(1024**2)

        # Streams
        self.had_ip = False
        self.have_ip = False

        # External executables
        self.tools = [
            "dhclient",
            "dhcpcd",
            "ifconfig",
            "iwconfig",
            "iwlist",
            "mii-tool",
            "pgrep",
            "ps",
            "tail",
            ]

        for cmd_name in self.tools:
            var_name = cmd_name.replace("-", "_")
            value = system.os_which(cmd_name)
            self.__dict__[var_name] = value

    def init_config(self):
        # reset program_name to match the executable that is running
        # without disturbing the already instantiated vars using program_name
        self.program_name = os.path.basename(sys.argv[0])
        
        lines = system.getfile(self.configfile)
        if not lines:
            raise Exception, "Failed to read configfile %s" % self.configfile
        lines = regex.replaceall("\s*;.*", "", lines).strip() # strip comments

        self.interfaces.clear()
        self.networks.clear()

        for medium in self.interface_media:
            ifaces = \
                self.read_config_block(lines, medium, self.interface_fields)
            for iface in ifaces:
                iface.medium = medium
            self.interfaces.extend(ifaces)

        nets = self.read_config_block(lines, "wireless_network", self.network_fields)
        self.networks.extend(nets)

    def read_config_block(self, s, block_name, fields):
        blocks = regex.findall("\[%s\]\s+((?:.+=.+\s*)+)" % (block_name), s)
        entries = sdictlist()
        for block in blocks:
            entry = sdict()
            for field in fields:
                value = regex.find1("\s*%s\s*=(.*)" % field, block)
                if value: entry[field] = value.strip()
    #       if len(entry) < fieldcount:
    #           msg = "In configuration file %s:\n"
    #           msg += "Failed to read config for '%s' block: "
    #           msg += "got %d field(s), expected %d"
    #           err_fg(msg % (configfile, block_name, len(entry), fieldcount))
    #           sys.exit(1)
            entries.append(entry)
        return entries



config = Config()
