#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import re
import time

from conf import config
import opts
import procs
import regex
from sdict import sdict
import ui


# Interface status

def read_iface(iface_name):
    fields = sdict(); info = sdict()
    fields.ip = "inet addr:([0-9.]+)"
    fields.up = "(UP)"
    output = procs.run("%s %s" % (config.ifconfig, iface_name))
    for key in fields.keys():
        info[key] = regex.find1(fields[key], output)
    return info

def has_ip(iface_name):
    invalid_range = "169.254."
    info = read_iface(iface_name)
    if info.up and info.ip and not regex.matches(invalid_range, info.ip):
        return info.ip

def iface_up(iface_name, ip=None):
    ip_str = ""
    if ip: ip_str = "inet %s" % ip
    info = read_iface(iface_name)
    if not info.up:
        procs.run("%s %s %s up" % (config.ifconfig, iface_name, ip_str))

def iface_down(iface_name):
    info = read_iface(iface_name)
    if info.up:
        procs.run("%s %s inet 0.0.0.0 down" % (config.ifconfig, iface_name))


# DHCP

def request_ip(iface_name):
    if config.dhcpcd:
        procs.kill_by_name(config.dhcpcd)
    procs.kill_by_name(config.dhclient)

    for i in config.interfaces:
        if i.interface != iface_name: iface_down(i.interface)
    iface_up(iface_name)

    if config.dhcpcd[0] == '/':
        procs.run("%s -d -t5 %s" % (config.dhcpcd, iface_name))
    else:
        procs.run("%s %s 2>&1" % (config.dhclient, iface_name))
    return has_ip(iface_name)


# Wired network

def wire_connected(iface_name):
    iface_up(iface_name)
    output = procs.run("%s %s" % (config.mii_tool, iface_name))
    link = regex.matches("link ok", output)
    if output and link: return iface_name


# Wireless network

wpa="""
IE: (?P<wpa_ver>.*%s.*)\s+
Group Cipher : (?P<group_cipher>[^\s]+)\s+
Pairwise Ciphers \(.\) : (?P<pairwise_ciphers>.+)\s+
Authentication Suites \(.\) : (?P<auth_suites>.+)\s*
(?P<preauth>Preauthentication Supported)*
"""
wpa = regex.replaceall("\n", "", wpa)
wpa_subfields = ['wpa_ver', 'group_cipher', 'pairwise_ciphers', 'auth_suites', 'preauth']

def read_wifi(iface_name):
    fields = sdict(); info = sdict()
    fields.assoc = "(IEEE 802.11)"
    fields.mode = "Mode:([^\s]+)"
    fields.essid = "ESSID:\"(.*?)\""
    fields.bssid = "Access Point: ([^\s]+)"
    fields.key = "Encryption key:([^\s]+)"
    output = procs.run("%s %s" % (config.iwconfig, iface_name))
    for key in fields.keys():
        info[key] = regex.find1(fields[key], output)
    if info.mode: info.mode = info.mode.lower()
    return info

def setup_wifi(iface_name, network=None):
    extra = ""
    if network:
        extra += "ap %s " % network.bssid
        extra += "essid \"%s\" " % network.essid
        
        key = "s:%s" % network.key
        if not network.key: key = "off"
        extra += "key %s " % key
    
    procs.run("%s %s mode managed %s" % (config.iwconfig, iface_name, extra))

    if network:
        deadline = time.time()+1.5
        while deadline > time.time():
            info = read_wifi(iface_name)
            if info.assoc: return True
            time.sleep(0.75)
        return False

def normal_scan(iface_name):
    output = ""
    deadline = time.time()+3
    while 1:
        if time.time() > deadline: break
        output += regex.replaceall("^.+\n" , "", single_scan(iface_name))
        time.sleep(0.5)
    return output

def single_scan(iface_name):
    procs.kill_by_name(config.iwlist)
    iface_up(iface_name)
    setup_wifi(iface_name)
    return procs.run("%s %s scan" % (config.iwlist, iface_name))

def read_scan(scan_data):
    # bump scan counter
    config.scan_count += 1

    if not scan_data:
        return config.live_networks


    blocks = scan_data.split("Cell")[1:]

    fields = sdict()
    fields.bssid = "Address: ([^\s]*)"
    fields.essid = "ESSID:\"(.*?)\""
    fields.channel = "Channel:([^\s]*)"
    fields.encrypted = "Encryption key:(on)"
    fields.bitrate = "Bit Rates:((?:[0-9.]* [Mbs;/]*\s*)*)"
    fields.quality = "Quality=([0-9]+/[0-9]+)"
    fields.signal = "Signal level=([^\s]* dBm)"
    fields.noise = "Noise level=([^\s]* dBm)"
    fields.beacon = "Last beacon: ([^\s]*ms)"

    for block in blocks:
        net = sdict()
        for key in fields.keys():
            s = regex.find1(fields[key], block)
            if s and s.strip(): net[key] = s.strip()

        # special handling for bitrate
        bitrates = regex.findall(fields.bitrate, block)
        if bitrates:
            bitrates = " ".join(bitrates)
            bitrates = regex.findall('[0-9.]+', bitrates)
            bitrates = map(float, bitrates)
            bitrates = sorted(bitrates)
            bitrate = bitrates[-1]
            bitrate = (int(bitrate) == float(bitrate)
                       and int(bitrate) or float(bitrate))
            net.bitrate = "%s Mb/s" % bitrate

        # convert quality into percentage
        if net.quality:
            st = net.quality.split('/')
            if len(st) == 2:
                (num, den) = st
                num, den = int(num), int(den)
                net.quality = "%s%%" % int( 100 * num / den )

        # extract wpa details
        fields.wpa = wpa % "WPA Version"
        fields.wpa2 = wpa % "WPA2 Version"
        for field in ['wpa', 'wpa2']:
            m = regex.find(fields[field], block)
            if m:
                wpa_info = sdict()
                for subfield in wpa_subfields:
                    wpa_info[subfield] = m.group(subfield)
                net[field] = wpa_info
    
        # shorthand security value for display
        if net.wpa and net.wpa2: net.sec = 'wpa/wpa2'
        elif net.wpa: net.sec = 'wpa'
        elif net.wpa2: net.sec = 'wpa2'
        elif net.encrypted and net.encrypted == "on":
            net.sec = 'wep'

        # record time
        net.last = time.time()

        config.live_networks = config.live_networks.merge1(net, 'bssid')


        # bump net counter
        def bump_freq(net):
            if 'freq' not in net:
                net.freq = 1
            else:
                net.freq = int(net.freq) + 1

        config.live_networks.set_with('bssid', net.bssid, bump_freq)

    config.live_networks = opts.post_negate_nets(config.live_networks, 'essid')
    return config.live_networks


if __name__ == "__main__":
    pass
    ui.init_config()
    #print config.networks
    #print has_ip("eth1")
    #print wire_connected("eth0")

#   print display_live_networks(read_scan(normal_scan("eth1")), field='sec')

#   print display_live_networks(read_scan(None))

#   curse("eth1",-1)

    nets = read_scan(single_scan("wlan0"))
#   nets = [n for n in nets if 'sec' not in n]
    print ui.display_live_networks(nets, column=3)

    #print request_ip("eth1")
    #print read_iface("eth1")
    #iface_up("eth1")
    #iface_down("eth1")
    #print read_wifi("eth1")
