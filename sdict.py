#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import regex


class sdictlist:
    def __init__(self, init_list=None, sort_key=None):
        self.lst = []
        self.sort_key = sort_key
        
        if init_list:
            for item in init_list:
                self.append(item)

    def isvalid(self, dict):
        if type(dict) == sdict:
            return True

    def __str__(self):
        return self.lst.__str__()

    def __len__(self):
        return len(self.lst)

    def __iter__(self):
        return iter(self.lst)

    def clear(self):
        self.lst = []

    def append(self, dict):
        if not self.isvalid(dict):
            raise Exception, "argument type of %s: %s invalid %s" %\
                (dict, type(dict), sdict)
        self.lst.append(dict)
        self = self.sort(lst=self.lst)

    def remove(self, index):
        try: del(self.lst[index])
        except: pass

    def extend(self, dl):
        items = dl.get_all()
        if items:
            for item in items:
                self.append(item)

    def get_top(self, pred=None):
        if pred: return filter(pred, self.lst)[0]
        return self.lst[0]

    def get_all(self, pred=None):
        if pred: return filter(pred, self.lst)
        return self.lst

    def set_with(self, keyname, keyval, f):
        f(filter(lambda dct: dct[keyname] == keyval, self.lst)[0])

    def sort(self, lst=None, sort_key=None, reverse=False):
        def compare(x, y):
            # insulate against unset keys
            if sort_key not in x and sort_key not in y: return 0
            elif sort_key not in x: return 1
            if sort_key not in y: return -1

            # numeric sort if string begins with digits
            m = regex.find1("(-?[0-9]*)", x[sort_key])
            n = regex.find1("(-?[0-9]*)", y[sort_key])
            if m and n:
                return cmp(int(n), int(m)) # reverse order to get desc

            return cmp(x[sort_key].lower(), y[sort_key].lower())

        if not lst: lst = self.lst
        if not sort_key: sort_key = self.sort_key
        lst = sorted(lst, compare)
        if reverse: lst.reverse()

        l = sdictlist()
        l.__dict__['lst'] = lst
        return l

    def ammend(self, old, new): # update old dict with new data
        if not new: return old
        for key in new.keys():
            if not new[key] or new[key] == "":
                new[key] = old[key]
        for key in [x for x in old.keys() if x not in new.keys()]:
            new[key] = old[key]
        return new

    # merge new_dict into dictlist
    def merge1(self, new_dict, key):
        dicts = self.lst
        found = False
        for (i, dict) in enumerate(dicts):
            if dict[key] == new_dict[key]:
                found = True
                dicts[i] = self.ammend(dict, new_dict)
        if not found:
            dicts.append(new_dict)
        return self.sort(lst=dicts)

    # merge new dictlist into dictlist
    def merge(self, new_dicts, key):
        dicts = self
        if not new_dicts: return dicts
        for new_dict in new_dicts:
            dicts.merge1(new_dict, key)
        return self.sort(lst=dicts)


class sdict(dict):
    def __init__(self, *args):
        dict.__init__(self)
        
        if args and len(args) == 1:
            (init_dict,) = args
            for (key, value) in init_dict.items():
                self.__setattr__(key, value)


    ## Helper methods
    
    def ___value_is_null___(self, value):

        # ignore null values and strings with only whitespace
        if not value or (type(value) == str and value.strip() == ""):
            return True


    # Dictionary interface
    
    def __setitem__(self, key, value):
        self.__setattr__(key, value)

    def __getitem__(self, key):
        return self.__getattribute__(key)

    def __delitem__(self, key):
        self.__delattr__(key)

    def clear(self):
        for key in dict.keys(self):
            self.__delitem__(key)

    def fromkeys(self, keys, value=None):
        if keys:
            new_sdict = sdict()
            for key in keys: new_sdict.__setitem__(key, value)
            return new_sdict

    def pop(self, key, default=None):
        value = self.__getitem__(key)
        self.__delitem__(key)
        if not value: return default
        return value

    def popitem(self):
        try:
            key = self.keys()[0]
            return (key, self.pop(key))
        except IndexError: return None

    def setdefault(self, key, value=None):
        v = self.__getitem__(key)
        if v: return v
        self.__setitem__(key, value)
        return value

    def update(self, pairs):
        for (key, value) in pairs:
            self.__setattr__(key, value)


    # Attribute interface
    
    def __setattr__(self, key, value):
        if not self.___value_is_null___(value):
            if type(value) == int: value = str(value)
            dict.__setattr__(self, key, value)
            dict.__setitem__(self, key, value)

    def __getattribute__(self, key):
        try:
            return dict.__getattribute__(self, key)
        except AttributeError: return None

    def __delattr__(self, key):
        try:
            dict.__delattr__(self, key)
            dict.__delitem__(self, key)
        except: pass



if __name__ == "__main__":
    d = sdict({'a':"3"})
    print "d.a:", d.a
    print "d['a']:", d['a']
    d['name'] = 'dd-wrt'
    d['a'] = '3'
    d.b = 1
    del(d['c'])
    d.__delattr__('c')

    print d
    d.clear()
    print d

    d2 = sdict()
    d2.a = "aa"
    d2.b = "ab"
    d2.c = 2
    
    d3 = sdict()
    d3.p = "fa"
    d3.b = "ce"
    d3.c = 3

    d4 = sdict()
    d4.p = "we"
    d4.z = "qw"
    d4.c = 9
    
    dl = sdictlist(sort_key="a")
    print dl.isvalid(d2)
    dl.append(d2)
    dl.append(d4)
    print dl

    print dl.get_top()
    print dl.get_top(pred=lambda x: x.a != None)

    dl2 = sdictlist(sort_key="c")
    dl2.append(d3)
    print dl2
    print dl.merge(dl2, "c")
