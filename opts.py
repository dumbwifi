#!/usr/bin/env python

# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.

import optparse
import sys

from conf import config
from output import logger
import regex
from sdict import sdict, sdictlist


# Read command line options

def create_parser():
    return optparse.OptionParser(add_help_option=None)

def read_opts(parser):
    def opts_help(option, opt_str, value, parser):
        print "Usage: %s [options]" % config.program_name
        for o in parser.option_list:
            var = o.metavar
            if not o.metavar: var = ""
            argument = "%s %s %s" % (o._short_opts[0], o._long_opts[0], var)
            print "  %s %s" % (argument.strip().ljust(25), o.help)
        sys.exit(0)
    
    parser.add_option("-h", "--help", action="callback", callback=opts_help,
        help="Display this message")
    opts = parser.parse_args()[0]
    config.opts.update( [(k, opts.__dict__[k]) for k in opts.__dict__] )

    # merge new nets info global list
    if config.opts.network:
        for (i, n) in enumerate(config.opts.network):
            pair = n.split(",")
            name = pair[0] ; pri = config.max_priortiy - i
            d = sdict({ 'essid': name, 'priority': pri })
            if len(pair) == 2: d['key'] = pair[1]
            config.networks = config.networks.merge1(d, 'essid')
    config.networks = negated_opts(config.networks, 'essid')

    # merge new interfaces into global list
    if config.opts.interface:
        for (i, n) in enumerate(config.opts.interface):
            pri = config.max_priortiy - i
            d = sdict({'interface': n, 'priority': pri})
            config.interfaces = config.interfaces.merge1(d, 'interface')
    config.interfaces = negated_opts(config.interfaces, 'interface')

def negated_opts(dicts, key):
    new_dicts = dicts
    for (i, d) in enumerate(dicts):
        val = regex.find1("^\^(.*)", d[key])
        if val:
            new_dicts.remove(i)
            new_dicts = sdictlist(init_list=new_dicts.get_all(\
                pred=lambda x: x[key] != val))
    return new_dicts

def post_negate_nets(nets, key):
    if not config.opts.network: return nets
    new_nets = nets
    for (i, n) in enumerate(config.opts.network):
        key_value = regex.find1("^\^(.*)[,]?.*", n)
        if key_value:
            new_nets = sdictlist(init_list=\
                new_nets.get_all(pred=lambda x: x[key] != key_value))
    return new_nets

