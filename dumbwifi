#!/usr/bin/env python
#
# Author: Martin Matusiak <numerodix@gmail.com>
# Licensed under the GNU Public License, version 3.
#
# <desc> Dumb network connection manager </desc>

import sys
import time

from conf import config
import network
import opts
from output import logger
import output
import procs
import ui


def init():
	# read config file
	ui.init_routine()

	# make sure we are the only instance
	procs.kill_previous_instances()

	# read command line options
	process_opts()

	# do sanity check
	if config.opts.checktools:
		ui.tools_check()

	# kill switch was triggered
	if config.opts.kill:
		sys.exit(0)

	# run in foreground if we want debug mode
	if not config.opts.debug and not config.opts.wardrive:
		procs.daemonize()

	# drop existing ip
	if config.opts.dropip or config.opts.wardrive:
		[network.iface_down(i.interface) for i in config.interfaces]

	# write our pid to the pidfile
	procs.write_pid()


def process_opts():
	p = opts.create_parser()
	p.add_option("-i", "--interface", dest="interface", action="append",
		metavar="iface", help="Prefer this interface")
	p.add_option("-n", "--network", dest="network", action="append",
		metavar="name[,key]", help="Prefer this wireless network")
	p.add_option("-w", "--wardrive", action="store_true",
		help="Attempt to connect to any open network in range")
	p.add_option("-d", "--debug", action="store_true",
		help="Run in foreground (debug mode)")
	p.add_option("-D", "--dropip", action="store_true",
		help="Release existing ip")
	p.add_option("-C", "--checktools", action="store_true",
		help="Do sanity check (check for missing tools)")
	p.add_option("-k", "--kill", action="store_true",
		help="Stop %s" % config.program_name)
	opts.read_opts(p)


def obtain_ip(wardrive=None):

	# make current state be previous state
	config.had_ip = config.have_ip

	# set output status
	if config.have_ip: logger.mute()
	else: logger.unmute()
	
	if ui.ip_check():
		config.have_ip = True
		return
	else: config.have_ip = False

	# if we had an ip on the previous run, but we just discovered it's gone,
	# we need to toggle screen output and re-run ip check
	if config.had_ip and not config.have_ip: return


	if not wardrive:
		prefer_wired = ui.choose_medium()

		if prefer_wired and ui.check_wired_link():
			# find top prority wired interface
			iface = config.interfaces.get_top(pred=lambda x: x.medium == "wired")
			if ui.request_ip(iface): return

	# find top prority wireless interface
	iface = config.interfaces.get_top(pred=lambda x: x.medium == "wireless")
	wifi_nets = ui.scan_wifi_networks(iface, wardrive=wardrive)

	if wifi_nets:
		for net in wifi_nets:
			if ui.request_ip(iface, net=net): return


def main():
	init()
	while 1:
		obtain_ip(wardrive=config.opts.wardrive)
		time.sleep(1)



if __name__ == "__main__":
	main()
